#include "Vector3.h"
#include <iostream> // In order to cout any error messages
#include <cmath> // Access power and square root functions

using namespace std;

// Declares an empty vector3
Vector3::Vector3(){}

// Declares a vector3 with given values
Vector3::Vector3(float x, float y, float z)
{
	this->x = x; // This grabs the vector we wish to assign values to
	this->y = y;
	this->z = z;
}

// Assign an existing vector to another vector
Vector3::Vector3(const Vector3& rhs)
{
	this->x = rhs.x; // This grabs the vector we are copying to
	this->y = rhs.y;
	this->z = rhs.z;
}

// Shorthand addition of vectors
void Vector3::operator+= (const Vector3 &v)
{
	// This will grab whatever is to the left of the +=, then point to each value as required
	this->x +=  v.x;
	this->y +=  v.y;
	this->z +=  v.z;
}

// Shorthand subtraction of vectors
void Vector3::operator-= (const Vector3 &v)
{
	// This will grab whatever is to the left of the -=, then point to each value as required
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
}

// Shorthand multiplication by scalar
void Vector3::operator*= (const float s)
{
	// This will grab whatever is to the left of the *=, then point to each value as required
	this->x *= s;
	this->y *= s;
	this->z *= s;
}

// Divide a vector by scalar
Vector3 Vector3::operator/ (const float s) const
{
	// Ensure you don't divide by zero to avoid NaN
	if (s != 0)
	{
		// Create resulting vector
		Vector3 result;
		result.x = this->x / s; // Assign each value to corresponding value of vector to the left of /, multiplied by given scalar
		result.y = this->y / s;
		result.z = this->z / s;
		return result; 
	}
	else // If attempting to divide by 0, show error message, don't return anything
	{
		cout << "Cannot divide by 0" << endl;	
	}
}

// Addition of two vectors
Vector3 Vector3::operator+ (const Vector3 &v) const
{   
	Vector3 result; // Create resulting vector
	result.x = this->x + v.x; // Use this to grab value next to + operator, add to corresponding value of given vector
	result.y = this->y + v.y;
	result.z = this->z + v.z;
	return result;	
}

// Subtraction of two vectors
Vector3 Vector3::operator- (const Vector3 &v) const
{
	Vector3 result; // Create resulting vector
	result.x = this->x - v.x; // Use this to grab value next to - operator, subtract from corresponding value of given vector
	result.y = this->y - v.y;
	result.z = this->z - v.z;
	return result;
}

// Multiplication of a vector by a scalar
Vector3 Vector3::operator* (const float s) const
{
	Vector3 result; // Create resulting vector
	result.x = this->x * s; // Use this to grab value next to * operator, multiply by each vector value found by this
	result.y = this->y * s;
	result.z = this->z * s;
	return result;
}

// Unary minus of a vector
Vector3 Vector3::operator- () const
{
	Vector3 result; // Create resulting vector
	result.x = -this->x; // Assign negative values found from this
	result.y = -this->y;
	result.z = -this->z;
	return result;
}

// Cross product of two vectors
Vector3 Vector3::Cross(const Vector3 &vA, const Vector3& vB)
{
	Vector3 result; // Create resulting vector
	result.x = (vA.y * vB.z) - (vA.z * vB.y); // Following cross product formula, assign each resulting vector value accordingly
	result.y = (vA.z * vB.x) - (vA.x * vB.z);
	result.z = (vA.x * vB.y) - (vA.y * vB.x);
	return result;
}

// Finds length of a vector
float Vector3::Length(const Vector3& v)
{
	float result; // Length is always a number
	return result = sqrt((pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2))); // Here we use square root and power functions accessed via cmath
}

// Returns length squared of a vector - as above with square root function removed
float Vector3::LengthSq(const Vector3& v)
{
	float result;
	return result = (pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2));
}

// Returns normalised vector
Vector3 Vector3::Normalize(const Vector3& v)
{
	Vector3 result; // Create resulting vector
	float len; // To store length of given vector, used later
	len = result.Length(v); // find length of given vector
	if (len != 0) // so long as length is not 0
	{
		result.x = v.x / len; // Divide each value by length
		result.y = v.y / len;
		result.z = v.z / len;
		return result;
	}
	else // In case length is 0, prevent division to avoid NaN
	{
		cout << "Cannot divide by 0" << endl; // Error message
	}
}

// Dot product of a vector - always a scalar
float Vector3::Dot(const Vector3 &vA, const Vector3& vB)
{
	float result; // To store scalar result
	return result = (vA.x * vB.x) + (vA.y * vB.y) + (vA.z * vB.z);	// Following dot product formula
}


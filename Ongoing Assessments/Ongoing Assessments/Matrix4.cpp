#include "Matrix4.h"
#include <math.h>

Matrix4::Matrix4() {}

Matrix4::Matrix4(const Matrix4& rhs)
{
	for (int i = 0; i < 16; i++)
		m[i] = rhs.m[i];
	
}

Matrix4::Matrix4(float _00, float _10, float _20, float _30, 
				 float _01, float _11, float _21, float _31,
				 float _02, float _12, float _22, float _32, 
				 float _03, float _13, float _23, float _33)
{
	m[0] = _00;	 m[4] = _10;  m[8] = _20;   m[12] = _30;
	m[1] = _01;	 m[5] = _11;  m[9] = _21;   m[13] = _31;
	m[2] = _02;	 m[6] = _12;  m[10] = _22;  m[14] = _32;
	m[3] = _03;  m[7] = _13;  m[11] = _23;  m[15] = _33;
}

float& Matrix4::operator[] (int index)
{
	return m[index];
}

const float& Matrix4::operator[] (int index) const
{
	return m[index];
}

Matrix4 Matrix4::Zero() 
{
	Matrix4 result;
	for (int i = 0; i < 16; i++)
		result[i] = 0.0f;
	return result;
}

Matrix4 Matrix4::Identity()
{
	Matrix4 result;
	for (int i = 0; i < 16; i++)
	{
		if (i % 5 == 0)
		{
			result.m[i] = 1;
		}
		else
		{
			result.m[i] = 0;
		}
	}
	return result;
}

Matrix4 Matrix4::Transpose(const Matrix4& mat)
{
	Matrix4 result;
	result.m[0] = mat.m[0];   result.m[4] = mat.m[1];   result.m[8] = mat.m[2];    result.m[12] = mat.m[3];
	result.m[1] = mat.m[4];   result.m[5] = mat.m[5];   result.m[9] = mat.m[6];    result.m[13] = mat.m[7];
	result.m[2] = mat.m[8];   result.m[6] = mat.m[9];   result.m[10] = mat.m[10];  result.m[14] = mat.m[11];
	result.m[3] = mat.m[12];  result.m[7] = mat.m[13];  result.m[11] = mat.m[14];  result.m[15] = mat.m[15];
	return result;
}

Matrix4 Matrix4::SetTranslation(const Vector3& translation)
{
	Matrix4 result = Identity();
	result.m[12] = translation.x;
	result.m[13] = translation.y;
	result.m[14] = translation.z;
	return result;
}

Vector3 Matrix4::GetTranslation(const Matrix4& mat)
{
	Vector3 result;
	result.x = mat.m[12];
	result.y = mat.m[13];
	result.z = mat.m[14];
	return result;
}

Matrix4 Matrix4::SetScale(const Vector3& scale)
{
	Matrix4 result = Identity();
	result.m[0] = scale.x;
	result.m[5] = scale.y;
	result.m[10] = scale.x;
	return result;
}

Matrix4 Matrix4::SetRotationAxis(const Vector3& axis, float angle)
{
	Matrix4 result = Identity();
	result.m[0] = cos(angle) + pow(axis.x, 2) * (1 - cos(angle));
	result.m[1] = axis.y * axis.x * (1 - cos(angle)) + axis.z * sin(angle);
	result.m[2] = axis.z * axis.x * (1 - cos(angle)) - axis.y * sin(angle);
	result.m[4] = axis.x * axis.y * (1 - cos(angle)) - axis.z * sin(angle);
	result.m[5] = cos(angle) + pow(axis.y, 2) * (1 - cos(angle));
	result.m[6] = axis.z * axis.y * (1 - cos(angle)) + axis.x * sin(angle);
	result.m[8] = axis.x * axis.z * (1 - cos(angle)) + axis.y * sin(angle);
	result.m[9] = axis.y * axis.z * (1 - cos(angle)) - axis.x * sin(angle);
	result.m[10] = cos(angle) + pow(axis.z, 2) * (1 - cos(angle));
	return result;
}

Vector3 Matrix4::TransformPoint(const Matrix4& mat, const Vector3& p)
{
	Vector3 result;
	result.x = (mat.m[0] * p.x) + (mat.m[4] * p.y) + (mat.m[8] * p.z) + mat.m[12];
	result.y = (mat.m[1] * p.x) + (mat.m[5] * p.y) + (mat.m[9] * p.z) + mat.m[13];
	result.z = (mat.m[2] * p.x) + (mat.m[6] * p.y) + (mat.m[10] * p.z) + mat.m[14];
	return result;
}

Vector3 Matrix4::TransformDirection(const Matrix4& mat, const Vector3& n)
{
	Vector3 result;
	result.x = (mat.m[0] * p.x) + (mat.m[4] * p.y) + (mat.m[8] * p.z);
	result.y = (mat.m[1] * p.x) + (mat.m[5] * p.y) + (mat.m[9] * p.z);
	result.z = (mat.m[2] * p.x) + (mat.m[6] * p.y) + (mat.m[10] * p.z);
	return result;
}



int main()
{
	Matrix4 a();

	Matrix4 b(0.0f, 4.0f, 8.0f, 12.0f,
		1.0f, 5.0f, 9.0f, 13.0f,
		2.0f, 6.0f, 10.0f, 14.0f,
		3.0f, 7.0f, 11.0f, 15.0f);

	Matrix4 c(b);

	float grabIndexValue = c[1];

	Matrix4 d = b.Zero;
		
		
		
}
#include "Vector3.h"

void main()
{
	// Declare empty vectors
	Vector3 add, sub, mul, div, neg, cross, norm;
	static Vector3 norm;
	// Float to store scalar result of dot product
	float dot, length, lengthSq;
	// Used these values to cross check with workbook question
	Vector3 v(-3.0f, 0.0f, 4.0f);
	Vector3 w(0.0f, 8.0f, -8.0f);
	Vector3 m(3.0f, 4.0f, 0.0f);
	// Required to access functions 
	Vector3 test;
	Vector3 unit(1.0f, 1.0f, 1.0f);
	Vector3 plusequals(1.0f, 2.0f, 3.0f);
	// Copy one vector to an empty
	Vector3 minusequals(plusequals);
	Vector3 mulequals(plusequals);
	// Testing multiply by scalar value	
	mul = unit * 5;
	// Testing addition of two vectors
	add = unit + mul;
	// Subtract two vectors
	sub = add - unit;
	// Divide vector by scalar
	div = mul / 5;
	// Unary minus of a vector
	neg = -unit;
	// =+ addition of two vectors
	plusequals += unit;
	// -= subtraction of two vectors
	minusequals -= unit;
	// *= multiplication of vector by scalar
	mulequals *= 5;
	// Dot product of two vectors
	dot = test.Dot(plusequals, plusequals);
	// Cross product of two vectors
	cross = test.Cross(v, w);
	// Length of a vector
	length = test.Length(m);
	// Length squared of a vector
	lengthSq = test.LengthSq(m);
	// Normalise a vector
	norm = test.Normalize(m);
	// Test divide by 0
	test = unit / 0;
}
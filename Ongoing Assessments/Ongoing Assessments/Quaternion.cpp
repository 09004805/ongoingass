#include "vector3.h"
#include "matrix4.h"
#include "quaternion.h"

#include <math.h> 
#include <stdio.h> 


Quaternion::Quaternion(float xx, float yy, float zz, float ww)
{
	x = xx;
	y = yy;
	z = zz;
	w = ww;
};

Quaternion::Quaternion(const Quaternion& rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	w = rhs.w;
}; 

Quaternion& Quaternion::operator=(const Quaternion& rhs)
{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		w = rhs.w;
	return *this;
}


Quaternion Quaternion::Identity()
{
	static Quaternion result(0, 0, 0, 1);
	return result;
}


Quaternion Quaternion::Conjugate(const Quaternion& q)
{
	Quaternion result;
	result.x = q.x * -1;
	result.y = q.y * -1;
	result.z = q.z * -1;
	result.w = q.w;
	return result;
}


float Quaternion::Length(const Quaternion& q)
{
	float result;	
	result = sqrt(pow(q.x, 2) + pow(q.y, 2) + pow(q.z, 2) + pow(q.w, 2));
	return result;
}


float Quaternion::LengthSq(const Quaternion& q)
{
	float result;
	result = pow(q.x, 2) + pow(q.y, 2) + pow(q.z, 2) + pow(q.w, 2);
	return result;
}


Quaternion Quaternion::Normalize(const Quaternion& q)
{
	float len = Length(q);
	Quaternion result;
	result.x = q.x / len;
	result.y = q.y / len;
	result.z = q.z / len;
	result.w = q.w / len;

	return result;
}


float Quaternion::Dot(const Quaternion& q1, const Quaternion& q2)
{
	float result;
	result = ((q1.x*q2.x) + (q1.y*q2.y) + (q1.z*q2.z) + (q1.w*q2.w));
	return result;
}


Quaternion Quaternion::FromAxisAngle(const Vector3& v, float angle)
{
	Quaternion result;
	result.x = v.x * sin(angle / 2);
	result.y = v.y * sin(angle / 2);
	result.z = v.z * sin(angle / 2);
	result.w = cos(angle / 2);
	return result;
}

// Couldn't work this out, using yours!
float Quaternion::ToAxisAngle(const Quaternion& q, Vector3& v)
{
	// The quaternion representing the rotation is
	//   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

	float sqrLength = q.x*q.x + q.y*q.y + q.z*q.z;
	if (sqrLength > 0.0f)
	{
		float invLength = 1.0f / sqrtf(sqrLength);

		v.x = q.x*invLength;
		v.y = q.y*invLength;
		v.z = q.z*invLength;

		return 2.0f*acosf(q.w);
	}
	else
	{
		// angle is 0 (mod 2*pi), so any axis will do.
		v.x = 1.0f;
		v.y = 0.0f;
		v.z = 0.0f;

		return 0.f;
	}
}// End ToAxisAngle(..)


Matrix4 Quaternion::ToRotationMatrix(const Quaternion& q)
{
	const float x2 = q.x*q.x;
	const float y2 = q.y*q.y;
	const float z2 = q.z*q.z;
	const float xy = q.x*q.y;
	const float xz = q.x*q.z;
	const float yz = q.y*q.z;
	const float wx = q.w*q.x;
	const float wy = q.w*q.y;
	const float wz = q.w*q.z;

	// filling up row-wise
	Matrix4 mat;
	mat[0] = 1.0f - 2.0f*(y2 + z2);		// [0][0]
	mat[4] = 2.0f*(xy - wz);				// [0][1]
	mat[8] = 2.0f*(xz + wy);				// [0][2]
	mat[12] = 0.f;						// [0][3]

	mat[1] = 2.0f*(xy + wz);				// [1][0]
	mat[5] = 1.0f - 2.0f*(x2 + z2);		// [1][1]
	mat[9] = 2.0f*(yz - wx);				// [1][2]
	mat[13] = 0.f;						// [1][3]

	mat[2] = 2.0f*(xz - wy);				// [2][0]
	mat[6] = 2.0f*(yz + wx);				// [2][1]
	mat[10] = 1.0f - 2.0f*(x2 + y2);		// [2][2]
	mat[14] = 0.f;						// [2][3]

	mat[3] = 0.f;						// [3][0]
	mat[7] = 0.f;						// [3][1]
	mat[11] = 0.f;						// [3][2]
	mat[15] = 1.f;						// [3][3]
	return mat;
}// End ToRotationMatrix(..)


Quaternion Quaternion::FromRotationMatrix(const Matrix4& m)
{
	Quaternion q(0, 0, 0, 1);

	float trace = m[0] + m[5] + m[10] + 1.f;

	float tolerance = 0.00001f;

	if (trace > tolerance)
	{
		float root = sqrtf(trace);
		q.w = 0.5f*root;
		root = 0.5f / root;
		q.x = (m[6] - m[9]) * root;
		q.y = (m[8] - m[2]) * root;
		q.z = (m[1] - m[4]) * root;
	}
	else
	{
		if (m[0] > m[5] && m[0] > m[10])
		{
			float root = sqrtf(m[0] - m[5] - m[10] + 1.f);
			q.x = 0.5f*root;
			root = 0.5f / root;
			q.y = (m[1] - m[4]) * root;
			q.z = (m[8] - m[2]) * root;
			q.w = (m[6] - m[9]) * root;
		}
		else if (m[5] > m[10])
		{
			float root = sqrtf(m[5] - m[0] - m[10] + 1.f);
			q.y = 0.5f*root;
			root = 0.5f / root;
			q.x = (m[1] - m[4]) * root;
			q.z = (m[6] - m[9]) * root;
			q.w = (m[8] - m[2]) * root;
		}
		else
		{
			float root = sqrtf(m[10] - m[0] - m[5] + 1.f);
			q.z = 0.5f*root;
			root = 0.5f / root;
			q.x = (m[8] - m[2]) * root;
			q.y = (m[6] - m[9]) * root;
			q.w = (m[1] - m[4]) * root;
		}
	}

	return q;
}// End FromRotationMatrix(..)


Quaternion Quaternion::Slerp(float t, const Quaternion& p, const Quaternion& q)
{
	Quaternion ret(0, 0, 0, 1);

	float cs = Quaternion::Dot(p, q);
	float angle = acosf(cs);

	if (abs(angle) > 0.0f)
	{
		float sn = sinf(angle);
		float invSn = 1.0f / sn;
		float tAngle = t*angle;
		float coeff0 = sinf(angle - tAngle)*invSn;
		float coeff1 = sinf(tAngle)*invSn;

		ret.x = coeff0*p.x + coeff1*q.x;
		ret.y = coeff0*p.y + coeff1*q.y;
		ret.z = coeff0*p.z + coeff1*q.z;
		ret.w = coeff0*p.w + coeff1*q.w;
	}
	else
	{
		ret.x = p.x;
		ret.y = p.y;
		ret.z = p.z;
		ret.w = p.w;
	}

	return ret;
}// End Slerp(..)






Quaternion operator*(const Quaternion& q1, const Quaternion& q2)
{
	return Quaternion(q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y,
		q1.w*q2.y + q1.y*q2.w + q1.z*q2.x - q1.x*q2.z,
		q1.w*q2.z + q1.z*q2.w + q1.x*q2.y - q1.y*q2.x,
		q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z);
} // End operator*(..)



Quaternion operator*(const Quaternion& q, float s)
{
	return Quaternion(q.x*s, q.y*s, q.z*s, q.w*s);
}// End operator*(..)


Quaternion operator*(float s, const Quaternion& q)
{
	return Quaternion(q.x*s, q.y*s, q.z*s, q.w*s);
}// End operator*(..)



Quaternion operator+(const Quaternion& q1, const Quaternion& q2)
{
	return Quaternion(q1.x + q2.x,
		q1.y + q2.y,
		q1.z + q2.z,
		q1.w + q2.w);
}// End operator*(..)





